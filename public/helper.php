<?php

/**
 * This is a helper page to call functions to be used throughout the PHP files and simplify/optimize coding.
 */

 function redirect($page){
    header("Location: $page");
    die();
 }

