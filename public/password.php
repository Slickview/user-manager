<?php

$password = $_POST['password'];
$hashed_password = password_hash($password, PASSWORD_DEFAULT);

if (password_verify($password, $hashed_password)) {
    // Success!
}
else {
    // Invalid login credentials
    echo "You've entered  invalid login credentials. You have 2 more tries before your account will be locked.";
}

/*
^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\W])(?=\S*[\d])\S*$

if ( trim( $password, 'a..z') != '' && trim( $password, 'A..Z') != '' && strlen($password) >= 8 )
{
  /* Password validation passes, do stuff. */
  /* echo "Success. You've created an account";
}
else {
  /* Password validation fails, show error. */
/*  echo "You do not meet the unqiue password requirements. <br> We require you meet the following criteria <br>
<ol>
<li>1 Uppercase Character</li>
<li>1 Lowercase Character</li>
<li>1 Special Character</li>
<li>1 Number Character</li>
<li>8 Characters Long</li>
</ol>  
  
<br>

Please create a new password that meet these requirements for your account to be updated.
  ";

}

*/


?>
