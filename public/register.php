<?php

//include 'dbconnect.php';
require_once('dbconnect.php');

//include helper.php to call all helper functions
include('helper.php');

//print_r($_POST);// die; // Die is like break in JS and stops all PHP afterwards

//print_r($connection);


// Connecting to DB

echo "<pre>";

// Email check Function 

function emailCheck($email)
{

    //Debug echo
    global $connection; // we need a global variable because the require was already used in this php file
    $sql = "select `ID` from `user-details` where `Email` ='" . $email . "'"; //'".$email."' = this is a string in the SQL DB
    $result = $connection->query($sql);

    /** connection is a $link -- since there can be more than one DB connected we want to make sure it is connected to the correct DB
     * SQL is a QUERY that the DB is searching in
     */
    $data = $result->fetch_assoc();

    if (!empty($data) && $data['ID']) {
        return FALSE;
    }
    return TRUE;
}

// User Exist Function
function userExist($firstName)

{
    global $connection;
    $sql = "select `ID` from `user-details` where 'First-Name' ='" . $firstName . "'";
    $result = $connection->query($sql);
    $data = $result->fetch_assoc(); // fetch_assoc ???? 

    if (!empty($data) && $data['ID']) {
        return false;
    }
    return true;
}



if (isset($_POST['submit'])) { // do not use !isset use isset




    // Make sure no empty fields -- cant create record without entering data
    if ($_POST['firstName'] != "" && $_POST['lastName'] != "" && $_POST['password'] != '' && $_POST['email'] != "") {

        // Debug section

        //echo "This is the Register.PHP";

        // Phone verification

        $phoneError = '';
        if (array_key_exists('phoneNumber', $_POST)) {
            if (!preg_match('/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/', $_POST['phoneNumber']) && !preg_match('/^[0-9]{3}[0-9]{3}[0-9]{4}$/', $_POST['phoneNumber'])) {
                $phoneError = 'Invalid Number format! Please use the correct format: ###-###-#### or ##########';
                redirect('register.html'); //redirect function ('the page to redirect too')
            }
        }

        //Email Exists Already
        $emailError = "";
        if (!emailCheck($_POST['email'])) // if IT DOESNT PASS TEST ABOVE THEN
        {
            $emailError = 'This email already exists, please choose another one.';
            redirect('register.html');
        }


        // User Exists Already
        $userError = "";

        if (!userExist($_POST['firstName'])) {
            $userError = 'This user already exists, please choose another one.';
            redirect('register.html');
        }


        // Hash Passwords

        $password = $_POST['password'];
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);

        if (password_verify($password, $hashed_password)) {

            // echo "This is the Password";
            // Success! -- Start SQL Testing
            // Push to server

            $sql = "INSERT INTO `user-details` (`First-Name`, `Last-Name`, `Phone`, `Email`, `Password`)  
            VALUES ('" . $_POST['firstName'] . "', '" . $_POST['lastName'] . "', '" . $_POST['phone'] . "', '" . $_POST['email'] . "', '" . $hashed_password . "')";

            // Debugging Test
            // print_r($sql);

            //If statement to check if the SQL queries is running to DB or not
            if ($connection->query($sql) === TRUE) {

                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $connection->error;
            }
        } else {
            // Invalid login credentials
        }
    }
}


//

/*

// how to post a message after a form has been submitted

if(isset($_POST['submit'])) // If the submit button is pressed

       { 
           //then do this
    
        echo "Welcome " + firstName; 
        echo "<br>";
        echo "Your username for this site is: " + email; 
       
}

else {
    // if the button is not pressed then do this
    
    echo "You did not submit the form properly.";
}


*/
